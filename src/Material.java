/**
 * Created by a on 2015-10-05.
 */
public enum Material {
    PHARMACEUTICALS, FOOD, ELECTRONICS, OTHER
}
