import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class PricingTest {
    @Test
    public void testGiven1() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(1299.99), 3, Material.FOOD);
        Assert.assertEquals(BigDecimal.valueOf(1591.58).compareTo(estimate), 0);
    }

    @Test
    public void testGiven2() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(5432), 1, Material.PHARMACEUTICALS);
        Assert.assertEquals(BigDecimal.valueOf(6199.81).compareTo(estimate), 0);
    }

    @Test
    public void testGiven3() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(12456.95), 4, Material.OTHER);
        Assert.assertEquals(BigDecimal.valueOf(13707.63).compareTo(estimate), 0);
    }

    @Test
    public void testNegativeBase() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(-1), 4, Material.OTHER);
        Assert.assertEquals(BigDecimal.ONE.negate().compareTo(estimate), 0);
    }

    @Test
    public void testZeroBase() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.ZERO, 4, Material.OTHER);
        Assert.assertEquals(BigDecimal.ONE.negate().compareTo(estimate), 0);
    }

    @Test
    public void testNegativePeople() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(12456.95), -4, Material.OTHER);
        Assert.assertEquals(BigDecimal.ONE.negate().compareTo(estimate), 0);
    }

    @Test
    public void testZeroPeople() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(12456.95), 0, Material.OTHER);
        Assert.assertEquals(BigDecimal.ONE.negate().compareTo(estimate), 0);
    }

    @Test
    public void testLargeBase() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(1000000000), 1, Material.OTHER);
        Assert.assertEquals(BigDecimal.valueOf(1062600000).compareTo(estimate), 0);
    }

    @Test
    public void testManyPeople() {
        BigDecimal estimate = Pricing.estimate(BigDecimal.valueOf(10), 1000000, Material.OTHER);
        Assert.assertEquals(BigDecimal.valueOf(126010.50).compareTo(estimate), 0);
    }
}
