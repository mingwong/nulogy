import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class Pricing {
    private static final BigDecimal MARKUP_FLAT = new BigDecimal(1.05);
    private static final BigDecimal MARKUP_PERSON = new BigDecimal(0.012);

    private static Map<Material, BigDecimal> materialMarkup = new HashMap<Material, BigDecimal>();

    static {
        materialMarkup.put(Material.PHARMACEUTICALS, new BigDecimal(0.075));
        materialMarkup.put(Material.FOOD, new BigDecimal(0.13));
        materialMarkup.put(Material.ELECTRONICS, new BigDecimal(0.02));
        materialMarkup.put(Material.OTHER, new BigDecimal(0));
    }

    public static boolean validate(BigDecimal base, int numPeople) {
        if (BigDecimal.ZERO.compareTo(base) >= 0) return false;
        else if (numPeople <= 0) return false;
        else return true;
    }

    public static BigDecimal estimate(BigDecimal base, int numPeople, Material productType) {
        return estimate(base, numPeople, productType, materialMarkup);
    }

    public static BigDecimal estimate(BigDecimal base, int numPeople, Material productType, Map<Material, BigDecimal> materialMarkupMap) {
        if (!validate(base, numPeople)) return BigDecimal.ONE.negate();
        else {
            BigDecimal flatMarkup = base.multiply(MARKUP_FLAT);
            BigDecimal peopleMarkup = flatMarkup.multiply(new BigDecimal(numPeople)).multiply(MARKUP_PERSON);
            BigDecimal materialMarkup = flatMarkup.multiply(materialMarkupMap.get(productType));

            return flatMarkup.add(peopleMarkup).add(materialMarkup).setScale(2, RoundingMode.HALF_UP);
        }
    }
}
